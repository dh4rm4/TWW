
was_dh4_k8p_innovate_or_die_dilemmas_triggered = 0;
was_dh4_innovate_or_die_starting_mission_triggered = 0;

local ANGRUND_CLAN="wh_main_dwf_karak_izor"

function dh4_rise_of_k8p()
   dh4_WriteToLog("[BOID] Get started.");

   dh4_WriteToLog("Before dilemma");
   --cm:trigger_dilemma_raw("wh_main_dwf_karak_izor", "dh4_k8p_innovate_or_die", true)
   dh4_WriteToLog("After dilemma");

   cm:add_faction_turn_start_listener_by_name(
	  "karak_faction_turn_start",
	  ANGRUND_CLAN,
	  function(context)
		 dh4_WriteToLog("REGISTERING CUSTOM BATTLEFIELD");
		 cm:add_custom_battlefield(
			"k8s_override",                                                                                 -- string identifier
			732,                                                                                                   -- x co-ord
			270,                                                                                                   -- y co-ord
			5000,                                                                                                      -- radius around position
			false,                                                                                          -- will campaign be dumped
			"",                                                                                                    -- loading override
			"script/battle/campaign_battle/k8p_battle_start.lua",   -- script override
			"",                                                                                                    -- entire battle override
			0,                                                                                                     -- human alliance when battle override
			false,                                                                                          -- launch battle immediately
			true,                                                                                           -- is land battle (only for launch battle immediately)
			false                                                                                           -- force application of autoresolver result
		 );
		 dh4_WriteToLog("SUCCESS REGISTERING CUSTOM BATTLEFIELD");

		 -- START FIRST DILEMMA
		 if was_dh4_k8p_innovate_or_die_dilemmas_triggered == 0 then
			local region = cm:get_region("wh_main_eastern_badlands_karak_eight_peaks");
			local owning_faction = region:owning_faction();

			if not owning_faction:is_null_interface() and owning_faction:name() == ANGRUND_CLAN then
			   dh4_WriteToLog("Try triggering dilemma");
			   cm:trigger_dilemma_raw("wh_main_dwf_karak_izor", "dh4_k8p_innovate_or_die", true);
			   was_dh4_k8p_innovate_or_die_dilemmas_triggered = 1;
			end;
		 -- INITIALISE FIRST MISSION
		 elseif was_dh4_innovate_or_die_starting_mission_triggered == 0 then
			dh4_WriteToLog("triggerring mission");
			cm:trigger_mission(ANGRUND_CLAN, "dh4_innovate_or_die_starting_mission", true);
			was_dh4_innovate_or_die_starting_mission_triggered = 1;
			dh4_WriteToLog("triggerring mission");
		 end;
	  end,
	  true
   );

   dh4_WriteToLog("[BOID] End here.");
end

--------------------------------------------------------------
----------------------- SAVING / LOADING ---------------------
--------------------------------------------------------------
cm:add_saving_game_callback(
   function(context)
	  cm:save_named_value("was_dh4_k8p_innovate_or_die_dilemmas_triggered", was_dh4_k8p_innovate_or_die_dilemmas_triggered, context);
	  cm:save_named_value("was_dh4_innovate_or_die_starting_mission_triggered", was_dh4_innovate_or_die_starting_mission_triggered, context);

   end
);

cm:add_loading_game_callback(
   function(context)
	  was_dh4_k8p_innovate_or_die_dilemmas_triggered = cm:load_named_value("was_dh4_k8p_innovate_or_die_dilemmas_triggered", 0, context);
	  was_dh4_innovate_or_die_starting_mission_triggered = cm:load_named_value("was_dh4_innovate_or_die_starting_mission_triggered", 0, context);
   end
);
