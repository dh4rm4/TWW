
-- Doc: https://chadvandy.github.io/tw_modding_resources/campaign/episodic_scripting.html#section:episodic_scripting:Next%20Battle%20&%20Campaign
function add_k8p_battle_script_override()
	cm:add_custom_battlefield(
		"k8s_override",											-- string identifier
		732,													-- x co-ord
		270,													-- y co-ord
		1,												        -- radius around position
		false,												-- will campaign be dumped
		"",													-- loading override
		"script/battle/campaign_battle/k8p_battle_start.lua",	-- script override
		"",													-- entire battle override
		0,													-- human alliance when battle override
		false,												-- launch battle immediately
		true,												-- is land battle (only for launch battle immediately)
		false												-- force application of autoresolver result
	);
end;

--------------------------------------------------------------
----------------------- ACTIVATE ON SAVE LOADING ---------------------
--------------------------------------------------------------
cm:add_loading_game_callback(
    function(context)
        was_dh4_k8p_innovate_or_die_dilemmas_triggered = cm:load_named_value("was_dh4_k8p_innovate_or_die_dilemmas_triggered", 0, context);
    end
);
