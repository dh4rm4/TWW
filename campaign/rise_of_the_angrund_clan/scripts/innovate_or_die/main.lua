was_dh4_k8p_innovate_or_die_dilemmas_triggered = 0;
was_dh4_innovate_or_die_starting_mission_triggered = 0;

local ANGRUND_CLAN="wh_main_dwf_karak_izor"

function dh4_rise_of_k8p()
    dh4_WriteToLog("[BOID] Get started.");

    dh4_WriteToLog("Before dilemma");
    --cm:trigger_dilemma_raw("wh_main_dwf_karak_izor", "dh4_k8p_innovate_or_die", true)
    dh4_WriteToLog("After dilemma");


    cm:add_faction_turn_start_listener_by_name(
	   "karak_faction_turn_start",
	   ANGRUND_CLAN,
	   function(context)

		  -- START INNOVATE OR DIE - DILEMMAS + CHAIN MISSIONS
		  if was_dh4_k8p_innovate_or_die_dilemmas_triggered == 0 then
			 local region = cm:get_region("wh_main_eastern_badlands_karak_eight_peaks");
			 local owning_faction = region:owning_faction();

			 if not owning_faction:is_null_interface() and owning_faction:name() == ANGRUND_CLAN then
				dh4_WriteToLog("Try triggering dilemma");
				cm:trigger_dilemma_raw("wh_main_dwf_karak_izor", "dh4_k8p_innovate_or_die", true);
				was_dh4_k8p_innovate_or_die_dilemmas_triggered = 1;
			 end;

			 -- Trigger first `innovate_or_die` mission if choice was made during previous dilemma
			 core:add_listener(
				"InnovateOrDie_choice_event",
				"DilemmaChoiceMadeEvent",
				function(context)
				   return context:dilemma() == "dh4_k8p_innovate_or_die"
				end,
				function(context)
				   local choice = context:choice();
				   if choice == 1
				   then
					  cm:trigger_mission(ANGRUND_CLAN, "dh4_innovate_or_die_starting_mission", true);
				   end;
				end, false);

			 -- In case player success the mission to recruit Grim Burloksson
			 -- 1. We spawn Grim Burloksson in Zhufbar
			 -- 2. Add massive diplomatic penalty with Zhufbar
			 -- 3. Break trade agreements with Zhufbar + an other faction
			 core:add_listener(
				"SuccesfulyRecruit_Grimm_Burloksson",
				"MissionSucceeded",
				function(context)
				   return context:mission():mission_record_key() == "dh4_innovate_or_die_move_to_zhufbar"
				end,
				function()
				   dh4_WriteToLog("SHOULD SPAWN GRIM BURLOKSSON HERE!");
				end,
				false
			 );
		  end;
	   end,
	   true
    );

    dh4_WriteToLog("[BOID] End here.");
end



--------------------------------------------------------------
----------------------- SAVING / LOADING ---------------------
--------------------------------------------------------------
cm:add_saving_game_callback(
    function(context)
        cm:save_named_value("was_dh4_k8p_innovate_or_die_dilemmas_triggered", was_dh4_k8p_innovate_or_die_dilemmas_triggered, context);
        cm:save_named_value("was_dh4_innovate_or_die_starting_mission_triggered", was_dh4_innovate_or_die_starting_mission_triggered, context);

    end
);

cm:add_loading_game_callback(
    function(context)
        was_dh4_k8p_innovate_or_die_dilemmas_triggered = cm:load_named_value("was_dh4_k8p_innovate_or_die_dilemmas_triggered", 0, context);
        was_dh4_innovate_or_die_starting_mission_triggered = cm:load_named_value("was_dh4_innovate_or_die_starting_mission_triggered", 0, context);
    end
);


function cpt_giraffes_dillemma()
    -- Dilemma choice listener
    core:add_listener(
        "CptGiraffeDilemmaChoiceMadeEvent",
        "DilemmaChoiceMadeEvent",
        function(context)
            return context:dilemma() == cpt_giraffes_dilemma; -- dilemma key
        end,
        function(context)
            local choice = context:choice();
            local faction_name = cm:get_faction("faction string");  -- faction key
            local ancillary_one = "ancillary one string";           -- ancillary key
            local ancillary_two = "ancillary two string";           -- ancillary key
            local ancillary_three = "ancillary three string";       -- ancillary key
            local ancillary_four = "ancillary four string";         -- ancillary key

            -- Checking if the dilemma choice made was the first one, note it starts at 0
            if choice == 0 then

                out("Dilemma Choice one chosen");

                -- Awarding ancillary one to the faction specified above, with an entry in the event feed
                cm:add_ancillary_to_faction(faction_name, ancillary_one, false);
                out("Awarded " .. ancillary_one .. " to " .. faction_name);

                -- Checking if the dilemma choice made was the second one, note it is labled 1
            elseif choice == 1 then

                out("Dilemma Choice two chosen");

                -- Awarding ancillary two to the faction specified above, with an entry in the event feed
                cm:add_ancillary_to_faction(faction_name, ancillary_two, false);
                out("Awarded " .. ancillary_two .. " to " .. faction_name);

            elseif choice == 2 then

                out("Dilemma Choice three chosen");
                -- Awarding ancillary three to the faction specified above, with an entry in the event feed
                cm:add_ancillary_to_faction(faction_name, ancillary_three, false);
                out("Awarded " .. ancillary_three .. " to " .. faction_name);

            elseif choice == 3 then

                out("Dilemma Choice four chosen");
                -- Awarding ancillary four to the faction specified above, with an entry in the event feed
                cm:add_ancillary_to_faction(faction_name, ancillary_four, false);
                out("Awarded " .. ancillary_four .. " to " .. faction_name);
            end;
        end,
        false
    );
end;



-- ADD CUSTOM SCRIPT FOR BATTLE
    -- cm:add_faction_turn_start_listener_by_name(
    --     "karak_faction_turn_start",
    --     ANGRUND_CLAN,
    --     function(context)
    --     dh4_WriteToLog("REGISTERING CUSTOM BATTLEFIELD");
    --     cm:add_custom_battlefield(
    --             "k8s_override",                                                                                 -- string identifier
    --             732,                                                                                                   -- x co-ord
    --             270,                                                                                                   -- y co-ord
    --             5000,                                                                                                      -- radius around position
    --             false,                                                                                          -- will campaign be dumped
    --             "",                                                                                                    -- loading override
    --             "script/battle/campaign_battle/k8p_battle_start.lua",   -- script override
    --             "",                                                                                                    -- entire battle override
    --             0,                                                                                                     -- human alliance when battle override
    --             false,                                                                                          -- launch battle immediately
    --             true,                                                                                           -- is land battle (only for launch battle immediately)
    --             false                                                                                           -- force application of autoresolver result
    --     );
    -- dh4_WriteToLog("SUCCESS REGISTERING CUSTOM BATTLEFIELD");


-- ADD UNDERCITY
-- local region = cm:get_region("wh_main_the_silver_road_the_pillars_of_grungni"):cqi();
-- local player_faction = cm:get_faction("wh_main_dwf_dwarfs"):command_queue_index();
-- cm:add_foreign_slot_set_to_region_for_faction(player_faction, region, "fuck");

-- ADD BUILDING TO UNDERCITY
-- local region = cm:get_region("wh2_main_charnel_valley_karag_orrud"):foreign_slot_managers();
-- local first_slot_cqi = region:item_at(0):slots():item_at(0):cqi();
-- cm:foreign_slot_instantly_upgrade_building(first_slot_cqi, "fuckone");

-- local player_faction = cm:get_faction("wh_main_dwf_dwarfs"):command_queue_index();
-- cm:add_foreign_slot_set_to_region_for_faction(player_faction, region, "fuck");
