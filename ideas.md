Ideas:
 - Dwarf merenaries (https://comiconlinefree.net/dwarves/issue-11/55)
 - have a gate mate that blocks the entrance to hodes of enemies (https://www.deviantart.com/che-rigas/art/Die-Zwerge-The-Dwarves-comic-page-1-colored-367351517)
 - add food / water / ammo management
 - free the captured heroes / kings
 - mission to confederate dwarves factions
 - racism / eugenism
 - launchable raids
 - a building to unlock the main vault of king lunn
 - multiple battle grounds:
    -> settlement:
       |- (all but skaven) main siege
       |- (skaven) underground
    -> Just front of settlements:
    	|- multiples small outpost with walls
    -> one small settlement on each side of the entrance of K8P
 - attack from spider army from spider notch caves
  -> bomb the entrance of the cave to spawn ack of spider units (it will attack ewverything)
 - to get the new gunpowder based units, must figh/compete with the guild of eingeener
  |-> for doing that, the lizard will consider going too far away from the great plan, and declare war on the player
 - Recruit mercenaries (oger, humans...) that spawn as allied armies
 - New building to restore all underway passages. This should provide just enough underway range bonus to allow moving from Holds to holds
   + add uniq bonus to k8p to serve as a hub between noth/south/east holds (already a landmark building their)OC

# Missions
## Explore lower depths of K8P and open vaults
  |-> find old grudges book: could store grudges against specific factions (to force player go fck them up)
  |-> need to recruit specific rune lords/heroes to be able to start those missions
  |-> allocate local units to the mission (more units -> + success rate)
  Good:
  	- gold/oathgold
  	- artifact
  	- gears (armor/weapon..)
  	- old runes to make Rune golems (start a specific chain of missions to complete the golem factory)
  Bad:
  	- loose units
  	- activate bad artifacts wanted by other factions: the player get a UI choice, to hand it to the faction (malus to faction's enemies + dwarfs), or keep it for no bonus and instant war or high diplomacy penalty.
  	- debuf for X turns (corruption/higher upkeep/de-growth/public order penalty)
## Innovate or die
  0. Higher new legendary diplomat lord
  1. Go ask permission to Karaz-a-Karak to try new stuff (get refused)
  2. Go buy materials/resources/ knowledges to Zhufbar (pay up lots of money) + get big debuff to Dwarfs factions for disobeing.
  3. build a new experimental workshop (instant penalty with all dwarfs factions + even more to zhufbar one (break treaties))
  4. Go recruit expulsed engineer around the map to let them develop their inventions.
  	|-> must move a lord close to specific cities (will force player to have good relation with local lords)
  	|-> must have specific units to prove engineer that you embrace inovations
  	|->
  (alternative): Find lost techs instructions by exploring lower depths (ex: golems, runes)
  5. Bring unit home
  6. Spend initial cost to develop new tech for specific engineer
  7. a. test prototype unit on battlefield
     b. Spend more gold
     c. Go fetch special resources around the map
     d. test prototype unit on battlefield
     The length of this cycle will depend on the tech to develop (ex: longer for gyro, less for gunpowders bonus etc...)
  8. Build new unit buildings (get even more penalty to dwarf zhufbar BUT bonus to Karaz-a-Karak)

  Inovation exemples:
  	- golems
  	- snipers rifles
  	- gatling gunners
  	- shotgun
  	- steam tanks (https://steamcommunity.com/sharedfiles/filedetails/?id=1980514782, https://steamcommunity.com/sharedfiles/filedetails/?id=2234604629)
  	- powder improvments
  	- better alloy (increase armored units speeds)
  	- different projectiles for artilleries
  	- different projectiles for ironbreakers
  	- Gyro:
  		  	- [Gyro carrier](https://steamcommunity.com/sharedfiles/filedetails/?id=2304651544)
  		  	- [Gatling Gyrocopter](https://steamcommunity.com/sharedfiles/filedetails/?id=2152656428)
  		  	- [Dragon Fire Gyrocopter](https://steamcommunity.com/sharedfiles/filedetails/?id=2167099776)
  		  	- [Dwarf airforce Regiments of Renown](https://steamcommunity.com/sharedfiles/filedetails/?id=2154274355)
  	- different projectiles for gyro
  	- new ale that regenerated health (Dwarf beer wagon)
  	- dwarf death roler: https://steamcommunity.com/sharedfiles/filedetails/?id=1520441831&searchtext=steam+tank+dwarf

  Engineers to recrut:
    - jorek grimm
	- Grimm Burloksson
	- Sven Hasselfriesian
	- Bardin Goreksson
	- Thorek Ironbrow


# Mod inspirations:

* [Diplomacy Rejection Consequences](https://steamcommunity.com/sharedfiles/filedetails/?id=1586646600)
* [Flamboyant's - Reclaiming Lost Dwarf Holds](https://steamcommunity.com/sharedfiles/filedetails/?id=1624662710)
* [Cataph's Kraka Drak: the Norse Dwarfs 2.0](https://steamcommunity.com/workshop/filedetails/?id=1181220751)
* [Lord/hero bodyguards](https://steamcommunity.com/sharedfiles/filedetails/?id=2166357082)
* [Flamboyant's - Strategic Regions](https://steamcommunity.com/sharedfiles/filedetails/?id=1654916308)
* [Lore Series: Ungrim Slayer Quests](https://steamcommunity.com/workshop/filedetails/?id=2214277505)
* [Flamboyant's- The Ruinous Powers (NEW EVENTS)](https://steamcommunity.com/sharedfiles/filedetails/?id=1767274407)
* [Chariots with custom animals](https://steamcommunity.com/sharedfiles/filedetails/?id=946922214)
* [Bonus when faction own specific settlements](https://steamcommunity.com/sharedfiles/filedetails/?id=1729947977)
* [Deployable Obstacles](https://steamcommunity.com/sharedfiles/filedetails/?id=1991283490)

# Enemies
 1. Sea Dragons: https://steamcommunity.com/sharedfiles/filedetails/?id=2376283097
 2. Giant  Razorgor (beastmen): https://steamcommunity.com/sharedfiles/filedetails/?id=2211174669
 3. Ghorgon (beastmen): https://steamcommunity.com/sharedfiles/filedetails/?id=2164875944
 4. Coatl (lizardman): https://steamcommunity.com/sharedfiles/filedetails/?id=2130710441
 5. Moulder's Menagerie (skavens): https://steamcommunity.com/sharedfiles/filedetails/?id=2122291692

# Unit variations

## Lords
## heroes

- Tammer: a dwarf riding a bear. Comme with pack a bear as unit (bery slow replinishment)

## Damage dealers

* maces: https://steamuserimages-a.akamaihd.net/ugc/956348816582787165/EA948A35E8DBA63B06FFA1C9ECB336B962C2E50B/


## Valaya Oath

"A dead dwarf cannot get drunk!"
Belegar, taking his Valaya Oath.

Valaya, Dwarf Ancestor Goddess of Home and Hearth, Healing and Brewing is the wife of both Grungni and Grimnir.
She is the founder of many Dwarf holds (including Karaz-a-Karak and Karak Eight Peaks) and the protector of the Dwarf race.

Belagar after loosing his wife when fleeing the fallen citadel of Karak Eight peaks, decided to create a new oath: the Valaya Oath.
Those who take the oath, are willing to be damn after their death, as they wish to save the dwarven race and reclaim every single one of their lost holds.

* Always provide aid to a wounded or ailing Dwarf.[1a]
* Always assist a Dwarf-friend in need.[1a]
* Always attend to the needs of the young.[1a]
* Always protect fellow Dwarfs from harm, especially at the hands of a Dwarf enemy.[1a]
* Never allow Dwarf ale to fall in the hands of enemies, unless to do so saves Dwarf lives.[1a]
* Never knowingly sell or otherwise distribute spoiled ale.[1a]

## Trade units against special production stuff.
 - krak drak: elite canon against mammoth
